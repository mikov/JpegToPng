package com.example.jpegtopng.model;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;

public class DataModel {

    private static String PATH_IMAGE = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).toString() + "/Camera";

    public Observable<List<String>> listOfPicturesNames() {

        return (Observable.fromCallable(new Callable<List<String>>() {
            @Override
            public List<String> call() {
                List<String> listImageNames = new ArrayList<>();
                File folder = new File(PATH_IMAGE);
                File[] files = folder.listFiles();

                if (files != null) {
                    for (File f : files) {
                        listImageNames.add(f.getName());
                    }
                }
                return listImageNames;
            }
        }));
    }

    public Observable converterJpegToPng(final String filepath) {

        String titleBuf = "";
        if (filepath.indexOf(".") > 0)
            titleBuf = filepath.substring(0, filepath.lastIndexOf("."));
        final String title = titleBuf;

        return Observable.create(new ObservableOnSubscribe() {
            @Override
            public void subscribe(ObservableEmitter emitter) throws InterruptedException {
                boolean success = false;
                try {
                    Bitmap bmp = BitmapFactory.decodeFile(PATH_IMAGE + "/" + filepath);
                    File convertedImage = new File(PATH_IMAGE + "/" + title + ".png");
                    FileOutputStream outStream = new FileOutputStream(convertedImage);
                    success = bmp.compress(Bitmap.CompressFormat.PNG, 100, outStream);
                    Thread.sleep(5000);
                    outStream.flush();
                    outStream.close();
                } catch (IOException e) {
                    emitter.onError(e);
                }
                if (success) emitter.onComplete();
                else emitter.onError(new Throwable("unsuccess"));
            }
        });
    }
}

