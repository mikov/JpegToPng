package com.example.jpegtopng.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.jpegtopng.model.DataModel;
import com.example.jpegtopng.view.IListPresenter;
import com.example.jpegtopng.view.ListRowView;
import com.example.jpegtopng.view.MainView;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

@InjectViewState
public class MainPresenter extends MvpPresenter<MainView> {

    private Disposable disposable;
    private DataModel dataModel = new DataModel();
    private ListPresenter listPresenter = new ListPresenter();

    class ListPresenter implements IListPresenter {
        List<String> strings = new ArrayList<>();

        @Override
        public void bindView(ListRowView view) {
            view.setText(strings.get(view.getPos()));
        }

        @Override
        public int getViewCount() {
            return strings.size();
        }

        @Override
        public void accept(String s) {
            Timber.d("MainPresenter");
            showCustomDialog();
            convertImage(s);
        }
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        getViewState().init();
        List<String> bufferList = new ArrayList<>();
        listPresenter.strings.addAll(bufferList);
        getViewState().updateList();
    }

    public void showExtDirFiles() {
        dataModel.listOfPicturesNames()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<String>>() {
                    @Override
                    public void accept(List<String> strings) throws Exception {
                        listPresenter.strings.clear();
                        listPresenter.strings.addAll(strings);
                        getViewState().updateList();
                    }
                });
    }

    public Disposable getDisposable() {
        return disposable;
    }

    private void convertImage(String path) {
        disposable = (Disposable) dataModel.converterJpegToPng(path)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver() {
                    @Override
                    public void onNext(Object o) {

                    }

                    @Override
                    public void onError(Throwable e) {
                        getViewState().closeDialog();
                        getViewState().showToast(e.toString());
                    }

                    @Override
                    public void onComplete() {
                        showExtDirFiles();
                        getViewState().closeDialog();
                        getViewState().showToast("Convertation success");
                    }
                });
    }

    private void showCustomDialog() {
        getViewState().showCustomDialog();
    }

    public ListPresenter getListFiles() {
        return listPresenter;
    }
}
