package com.example.jpegtopng.view;

public interface ListRowView {
    int getPos();
    void setText(String text);
}

