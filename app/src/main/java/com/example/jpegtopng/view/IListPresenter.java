package com.example.jpegtopng.view;

import io.reactivex.functions.Consumer;

public interface IListPresenter extends Consumer<String> {
    void bindView(ListRowView view);
    int getViewCount();

    @Override
    void accept(String s) throws Exception;
}


